﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.AccessControl;

namespace Prime_Number_Exercise
{
  class Program
  {
    static void Main(string[] args)
    {
      int PrimeNumbersLimit = 100;
      int CountFactors = 0;
      List<int> PrimeNumbers = new List<int>();

      for (int i = 1; i < PrimeNumbersLimit; i++)
      {
        for (int j = i; j > 0; j--)
        {

          if (i%j==0)
          {
            CountFactors++;
          }

        }

        if (CountFactors == 2)
          {
            PrimeNumbers.Add(i);
          }
        CountFactors = 0;
      }

      string path = @"C:\PrimeNumbers";
      string fileName = "PrimeNumbers.txt";
      
      
      Directory.CreateDirectory(path);
      DirectoryInfo d = new DirectoryInfo(path);
      if (d.Exists)
      {
        string FilePath = Path.Combine(path, fileName);
        File.WriteAllText(FilePath, string.Join(' ', PrimeNumbers));
        Console.WriteLine("File created successfully to this path ---> " + FilePath);
      }
      else
      {
        Console.WriteLine(string.Join(' ', PrimeNumbers));
      }
  
    
    
      Console.ReadLine();
   }
  }
}
